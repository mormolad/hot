'use strict';
let mqtt = require('mqtt');  // подключем модуль для mqtt
let fs = require('fs');  // модуль для работы с файловой системой
//let format = require('node.date-time'); //модуль для использования даты и время
let url = 'mqtt://localhost:1883'; //ip mqtt-брокера на 1883 порту

let ppm = null; //показания ппм-метра
let ppmSet = 1200; // ppm впоследствии замениться на ЕС
let ppmImpact = 1; // коэфициент перевода отклонения ппм от нормы во время воздействия, подбирается эмпирически
//let ppmOn = null;



let pH = null;
let pHSetHigh = 5.6;
let pHSetLow = 5.4;
let pHImpact = 1; // коэфициент перевода отклонения пШ от нормы во время воздействия, подбирается эмпирически
// let pHOnHigher = null;
// let pHOnLower = null;




// let level = null;
// let levelDeltaTime = 2; // подбирается эмпирически или в зависимости от производительности насоса
// let levelOn = null;


let tempAir = null;
let tempSetHighAir = 25;



let tempWater = null;
let tempSetHighWater = 18;
let tempSetLowWater = 19;
// let tempOnHigherWater = null;
// let tempOnLowerWater = null;


let autoEndAk = 1;
// let error = null;

let client = mqtt.connect(url, {username: '2', password: '2'});   //подключение к mqtt брокеру
console.log('== mqtt started ==')

client.on('connect', function () {   
console.log('mother connected');
client.subscribe('/pH');
client.subscribe('/ppm');
client.subscribe('/temp-0');
client.subscribe('/temp-1');
//client.subscribe('/level');
client.subscribe('/pHOnHigher');
client.subscribe('/pHOnLower');
client.subscribe('/ppmOnHigher');
//client.subscribe('/levelOn');
client.subscribe('/tempOnHigherWater');
client.subscribe('/tempOnLowerWater');
client.subscribe('/tempOnLowerAir');
client.subscribe('/error');
});
client.on('close', function () { // надпись если нет подключения к брокеру
  console.log('mother DISCONNECT');
});

function onMqttMessage(topic, message) {
  if (topic === '/ppm') { // топик сравнивается, если ппм то записывается
       ppm = message;
      }else if (topic === '/pH') { // топик сравнивается, если пШ
        pH = message;
      }else if (topic === '/temp-0') { //'/tempAir' и так далее, если не понятно могу расписать
        tempAir = message;
      }else if (topic === '/temp-1') { //'/tempwater' и так далее, если не понятно могу расписать
        tempWater = message;
      } else if (topic === '/level') {
        level = message;
      } else if (topic === '/error') {
        error = message;
      } else if (topic === '/ppmOnHigher') {
        ppmOn = message;
      } else if (topic === '/pHOnHigher') {
        pHOnHigher = message;
      } else if (topic === '/pHOnLower') {
        pHOnLower = message;
      } else if (topic === '/levelOn') {
        levelOn = message;
      } else if (topic === '/tempOnHigherWater') {
        tempOnHigher = message;
      } else if (topic === '/tempOnLowerWater') {
        tempOnLower = message;
      }
  };


function autoEndAkF (){ // переберает шаги для поочерёдной проверки и корректировки ппм, пШ, уровня, температуры
  //разкоментить для отладки
 // console.log('step:', autoEndAk, '\nppm:', ppm, '     on/off:', ppmOn, '\npH:', pH, '   high ON', pHOnHigher, '   low On', pHOnLower, '\ntemp:', temp, '   temp high', tempOnHigher, '   temp low', tempOnLower, '\nlevel:', level, '    level ON:', levelOn, '\nerror = ', error);
    if (autoEndAk == 1){
      client.publish('/ppmOnHigher', 'off')
      setTimeout(ppmCorr, 300*1000);    //время задержки на размешивание 5мин
    } else if (autoEndAk == 2){
      client.publish('/pHOnLower', 'off')
      client.publish('/pHOnHigher', 'off')
      setTimeout(pHCorr, 300*1000); //время задержки на размешивание 5мин
    } else if (autoEndAk == 3){
      setTimeout(levelCorr, 1*1000);
    } else if (autoEndAk == 4){
      setTimeout(tempCorrAir, 1*1000);     
    } else if (autoEndAk == 5){
      setTimeout(tempCorrWater, 1*1000);
    }
};

function ppmCorr () { // функция добовления компота
 let ppmDeltaTime = (ppm - ppmSet) * ppmImpact; // пропорциональная состовляющая воздействия на ппм
  if (typeof ppm!='number'){ //сообщит об ошибке если нет показаний цисловых
    client.publish('/error', 'проблемы с ппм');
    autoEndAk =2;
    autoEndAkF();
    } else if (ppm < ppmSet) {
      client.publish('/ppmOnHigher', 'on')
      setTimeout(autoEndAkF, ppmDeltaTime*1000); // время подачи компота
    } else {
      autoEndAk +=1;
      autoEndAkF();
    }
  };


function pHCorr () { // функция корректировки пШ
  let pHDeltaTime = null;
  if (typeof pH!='number'){//сообщит об ошибке если нет показаний цисловых
    client.publish('/error', 'проблемы с пШ');
    autoEndAk =3;
    autoEndAkF();
    } else if (pH > pHSetHigh) {
      pHDeltaTime = (pH - pHSetHigh) * pHImpact; // пропорциональная состовляющая воздействия на пШ при высоком пШ
      client.publish('/pHLower', 'on');
      setTimeout(autoEndAkF, pHDeltaTime*1000);
    } else if (pH < pHSetLow){
      pHDeltaTime = (pHSetLow - pH) * pHImpact; // пропорциональная состовляющая воздействия на пШ при низком пШ
      client.publish ('/pHOnHigher', 'on');
      setTimeout(autoEndAkF, pHDeltaTime*1000);
    } else {
      autoEndAk +=1;
      autoEndAkF();
    }
  };

  function levelCorr () { // функция пополнения уровня
    if (typeof level!='number'){//сообщит об ошибке если нет показаний цисловых
    client.publish('/error', 'проблемы с уровнем');
    autoEndAk = 4;
    autoEndAkF();
    } else if (level == 1) {
      client.publish('/levelOn', 'on')
      setTimeout(autoEndAkF, 1*1000); // 1* количество секунд
    } else {
      client.publish('/levelOn', 'off')
      autoEndAk +=1;
      setTimeout(autoEndAkF, 1*1000);  // 1* количество секунд
    }
  };


  function tempCorrAir () { // функция корректировки температуры воды
    if (tempAir < 18 ||typeof tempAir!='number'){//сообщит об ошибке если нет показаний цисловых или грубо вышли за диапазоны регулирования
      client.publish('/error', 'проблемы с температурой воду');
      autoEndAk = 1;
      setTimeout(autoEndAkF, 1*1000);  // 1* количество секунд
      } else if (tempAir > tempSetHighAir) {
        client.publish('/tempOnLowerAir', 'on')
        setTimeout(autoEndAkF, 1*1000); // 1* количество секунд
      } else if (tempAir > (tempSetHighAir-1)){
        client.publish('/tempOnHigherAir', 'off')
        autoEndAk +=1;
        setTimeout(autoEndAkF, 2*1000);
      }
    };

  function tempCorrWater () { // функция корректировки температуры воды
  if (tempWater < 18 || tempWater > 19 || typeof tempWater!='number'){//сообщит об ошибке если нет показаний цисловых или грубо вышли за диапазоны регулирования
    client.publish('/error', 'проблемы с температурой воду');
    autoEndAk = 1;
    setTimeout(autoEndAkF, 1*1000);  // 1* количество секунд
    } else if (tempWater > tempSetHighWater) {
      client.publish('/tempOnLowerWater', 'on')
      setTimeout(autoEndAkF, 1*1000); // 1* количество секунд
    } else if (tempWater < tempSetLowWater){
      client.publish ('/tempOnHigherWater', 'on')
      setTimeout(autoEndAkF, 1*1000); // 1* количество секунд
    } else {
      client.publish('/tempOnLowerWater', 'off')
      client.publish('/tempOnHigherWater', 'off')
      autoEndAk = 1;
      setTimeout(autoEndAkF, 2*1000);
    }
  };

  setTimeout(autoEndAkF, 20*1000);
client.on('message', onMqttMessage); // срабатывает при появлении нового сообщения в брокере     
module.exports = client;