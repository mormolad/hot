load('api_timer.js');
load('api_arduino_onewire.js');
load('api_arduino_dallas_temp.js');
load('api_events.js');
load('api_mqtt.js');
load('api_timer.js');
load('api_sys.js');
load('api_config.js');
load('api_gpio.js'); // <==== 
load('api_ads1015.js');


let pin = 14;
let ow = OneWire.create(pin);
let dt = DallasTemperature.create(ow);
dt.begin();
let n = 0;
let sens = [];




if (n === 0) {
   n = dt.getDeviceCount();
   print('Sensors found:', n);

   for (let i = 0; i < n; i++) {
      sens[i] = '01234567';
      if (dt.getAddress(sens[i], i) === 1) {
         print('Sensor#', i, 'Address:', dt.toHexStr(sens[i]));
      }
   }
}

Timer.set(1000, Timer.REPEAT, function () {


      dt.requestTemperatures();

        for (let i = 0; i < n; i++) {
		let mensagem = JSON.stringify(dt.getTempC(sens[i]));
		let topic = '/temp-' + JSON.stringify(i);
		
    MQTT.pub(topic, mensagem, 0);

        print(topic, mensagem);
      }
  }, null);




 
let ADS1X15_I2C_addresss = 0x48 ;
let kPH = 0.0006; // пересчёт вольт в пШ
let kPpm = 0.1153846153846154; // пересчёт вольт в ппм
// Initialize Adafruit ADS1115 library
let ads = Adafruit_ADS1015.create_ads1115(ADS1X15_I2C_addresss);
ads.setGain(0x0000);         // 2/3x gain +/- 6.144V  1 bit = 3mV      0.1875mV (default)
ads.begin();

Timer.set(1000, true , function() {

	let adc0 = JSON.stringify((ads.readADC_SingleEnded(0)*kPH));
	let adc1 = JSON.stringify((ads.readADC_SingleEnded(1)*kPpm));
	let adc2 = JSON.stringify(ads.readADC_SingleEnded(2));
	let adc3 = JSON.stringify(ads.readADC_SingleEnded(3));

	print('pH: ',adc0,' ppm: ',adc1,' res: ',adc2,' res: ',adc3);

	MQTT.pub('/pH', adc0, 0);
	MQTT.pub('/ppm', adc1, 0);

    let pin0 = JSON.stringify(GPIO.read(0));
    let pin2 = JSON.stringify(GPIO.read(2));
      MQTT.pub('/levelCorr', pin0, 0);
      MQTT.pub('/levelAlarm', pin2, 0);


}, null);


